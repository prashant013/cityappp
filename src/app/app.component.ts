import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Http} from '@angular/http';
import PouchDB from 'pouchdb';
import 'rxjs/add/operator/map';

import { HomePage } from '../pages/home/home';
// import {count} from "rxjs/operator/count";
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;
// .constant('base_url', 'http://doc.cityremit.com/')


  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public http: Http) {
    // var base_url: 'http://doc.cityremit.com/';
    // this.http = http;


    platform.ready().then(() => {

      // return{
      //   initDB : initDB,
      //   /*populateDistricts : populateDistricts,
      //    populateCountries : populateCountries,
      //    populatePages : populatePages,
      //    */
      //   populateBeneficiaries: populateBeneficiaries,
      //   updateServiceFee: updateServiceFee,
      //   getAgentList: getAgentList,
      //   populateAgentList: populateAgentList,
      //   dropAgentList: dropAgentList,
      //   retrieveAgentList: retrieveAgentList,
      //
      //   getBeneficiaries: getBeneficiaries,
      //   // populateBeneficiaries: populateBeneficiaries,
      //   dropBeneficiaries: dropBeneficiaries,
      //
      //   getNews: getNews,
      //   populateNews: populateNews,
      //   dropNews: dropNews,
      //
      //   selectDistricts : selectDistricts,
      //   selectCountries : selectCountries,
      //   selectPages : selectPages,
      //   selectNews : selectNews,
      //   selectOccupation : selectOccupation,
      //   selectPurposeOfRemittance : selectPurposeOfRemittance,
      //   selectRelationship : selectRelationship,
      //   selectModeOfReceipt : selectModeOfReceipt,
      //   selectIdentificationDocument : selectIdentificationDocument,
      //   selectAgentList: selectAgentList,
      //   selectBeneficiaries : selectBeneficiaries,
      //
      //   getMasterData : getMasterData,
      //   selectPage: selectPage
      // };


        var db = new PouchDB('Countries', {adapter: 'websql'})
        var db1 = new PouchDB('Districts', {adapter: 'websql'})
        var db2 = new PouchDB('Pages', {adapter: 'websql'})
        var db3 = new PouchDB('News', {adapter: 'websql'})
        var db4 = new PouchDB('Occupation', {adapter: 'websql'})
        var db5 = new PouchDB('Purpose of Remittance', {adapter: 'websql'})
        var db6 = new PouchDB('Relationship', {adapter: 'websql'})
        var db7 = new PouchDB('Mode of Receipt', {adapter: 'websql'})
        var db8 = new PouchDB('Identification document', {adapter: 'websql'})
        var db9 = new PouchDB('Agent List', {adapter: 'websql'})
        var db0 = new PouchDB('Beneficiaris', {adapter: 'websql'})

        console.log("database created: ");
      


      function getCountry() {
        var count;
        //select
        return db.allDocs({
        include_docs: true,
        descending: true,
        attachments: true
        }).then(function (res) {
          // handle result
          console.log("count",res.rows.length)
          count = res.rows.length;
          return count;
        })
      }

      function getDistrict() {
        var count1;
        //select
        return db1.allDocs({
          include_docs: true,
          descending: true,
          attachments: true
        }).then(function (res) {
          // handle result
          console.log("count1",res.rows.length)
          count1 = res.rows.length;
          return count1;
        })
      }
      function getpage() {
        var count2;
        //select
        return db2.allDocs({
          include_docs: true,
          descending: true,
          attachments: true
        }).then(function (res) {
          // handle result
          console.log("count2",res.rows.length)
          count2 = res.rows.length;
          return count2;
        })
      }
      function getnews() {
        var count3;
        //select
        return db3.allDocs({
          include_docs: true,
          descending: true,
          attachments: true
        }).then(function (res) {
          // handle result
          console.log("count3",res.rows.length)
          count3 = res.rows.length;
          return count3;
        })
      }
      function getoccupation() {
        var count4;
        //select
        return db4.allDocs({
          include_docs: true,
          descending: true,
          attachments: true
        }).then(function (res) {
          // handle result
          console.log("count4",res.rows.length)
          count4 = res.rows.length;
          return count4;
        })
      }
      function getPurposeOfRemittance() {
        var count5;
        //select
        return db5.allDocs({
          include_docs: true,
          descending: true,
          attachments: true
        }).then(function (res) {
          // handle result
          console.log("count5",res.rows.length)
          count5 = res.rows.length;
          return count5;
        })
      }
      function getRelationship() {
        var count6;
        //select
        return db6.allDocs({
          include_docs: true,
          descending: true,
          attachments: true
        }).then(function (res) {
          // handle result
          console.log("count6",res.rows.length)
          count6 = res.rows.length;
          return count6;
        })
      }
      function getModeOfReceipt() {
        var count7;
        //select
        return db7.allDocs({
          include_docs: true,
          descending: true,
          attachments: true
        }).then(function (res) {
          // handle result
          console.log("count7",res.rows.length)
          count7 = res.rows.length;
          return count7;
        })
      }
      function getIdentificationDocument() {
        var count8;
        //select
        return db8.allDocs({
          include_docs: true,
          descending: true,
          attachments: true
        }).then(function (res) {
          // handle result
          console.log("count8",res.rows.length)
          count8 = res.rows.length;
          return count8;
        })
      }
      function getAgentList() {
        var count9;
        //select
        return db9.allDocs({
          include_docs: true,
          descending: true,
          attachments: true
        }).then(function (res) {
          // handle result
          console.log("count9",res.rows.length)
          count9 = res.rows.length;
          return count9;
        })
      }

      function getBeneficiaries() {
        var count0;
        //select
        return db0.allDocs({
          include_docs: true,
          descending: true,
          attachments: true
        }).then(function (res) {
          // handle result
          console.log("count0",res.rows.length)
          count0 = res.rows.length;
          return count0;
        })
      }



        var countryData = {
          keyword: 'countries'
        }
        var districtData = {
          keyword: "district"
        }
        var pageData = {
          keyword: "pages"
        }
        var newsData = {
          keyword: "news_events"
        }

        var occupationData = {
          keyword: "occupation"
        }
        var remitPurposeData = {
          keyword: "remit_purpose"
        }
        var relationshipData = {
          keyword: "relationship"
        }
        var receiptModeData = {
          keyword: "receipt_mode"
        }
        var identificaitonDocumentData = {
          keyword: "id_documenttype"
        }
        var agentData = {
          keyword: "agents"
        }


        getCountry().then(function (data) {
          console.log("Get: ", data)
          var count = data;
          if (count == 0) {
            http.put("http://doc.cityremit.com/api/master", countryData)
              .map(res => res.json())
              .subscribe((dato) => {
                console.log("Put country:", dato)
                for (let data of dato) {
                  console.log("loop wala", data);
                  populateCountries(data);
                }
                //loop halne
              }, error => {
                console.log("Error on retrieving countries", error);
              })
          }
        })

        getDistrict().then(function (data) {
          console.log("Get1: ", data)
          var count = data;
          if (count == 0) {
            http.put("http://doc.cityremit.com/api/master", districtData)
              .map(res => res.json())
              .subscribe((dato) => {
                console.log("Put district:", dato)
                for (let data of dato) {
                  console.log("loop wala1", data);
                  populateDistricts(data);
                }
                //loop halne
              }, error => {
                console.log("Error on retrieving district", error);
              })
          }
        })

        getpage().then(function (data) {
          console.log("Get2: ", data)
          var count = data;
          if (count == 0) {
            http.put("http://doc.cityremit.com/api/master", pageData)
              .map(res => res.json())
              .subscribe((dato) => {
                console.log("Put page:", dato)
                for (let data of dato) {
                  console.log("loop wala2", data);
                  populatePages(data);
                }
                //loop halne
              }, error => {
                console.log("Error on retrieving page", error);
              })
          }
        })

        getnews().then(function (data) {
          console.log("Get3: ", data)
          var count = data;
          if (count == 0) {
            http.put("http://doc.cityremit.com/api/master", newsData)
              .map(res => res.json())
              .subscribe((dato) => {
                console.log("Put news:", dato)
                for (let data of dato) {
                  console.log("loop wala3", data);
                  populateNews(data);
                }
                //loop halne
              }, error => {
                console.log("Error on retrieving news", error);
              })
          }
        })

        getoccupation().then(function (data) {
          console.log("Get4: ", data)
          var count = data;
          if (count == 0) {
            http.put("http://doc.cityremit.com/api/master", occupationData)
              .map(res => res.json())
              .subscribe((dato) => {
                console.log("Put occupation:", dato)
                for (let data of dato) {
                  console.log("loop wala4", data);
                  populateoccupation(data);
                }
                //loop halne
              }, error => {
                console.log("Error on retrieving occupation", error);
              })
          }
        })

        getPurposeOfRemittance().then(function (data) {
          console.log("Get5: ", data)
          var count = data;
          if (count == 0) {
            http.put("http://doc.cityremit.com/api/master", remitPurposeData)
              .map(res => res.json())
              .subscribe((dato) => {
                console.log("Put remittance:", dato)
                for (let data of dato) {
                  console.log("loop wala5", data);
                  populatePurposeOfRemittance(data);
                }
                //loop halne
              }, error => {
                console.log("Error on retrieving remittance", error);
              })
          }
        })

        getRelationship().then(function (data) {
          console.log("Get6: ", data)
          var count = data;
          if (count == 0) {
            http.put("http://doc.cityremit.com/api/master", relationshipData)
              .map(res => res.json())
              .subscribe((dato) => {
                console.log("Put relationship:", dato)
                for (let data of dato) {
                  console.log("loop wala6", data);
                  populateRelationship(data);
                }
                //loop halne
              }, error => {
                console.log("Error on retrieving relationship", error);
              })
          }
        })

        getModeOfReceipt().then(function (data) {
          console.log("Get7: ", data)
          var count = data;
          if (count == 0) {
            http.put("http://doc.cityremit.com/api/master", receiptModeData)
              .map(res => res.json())
              .subscribe((dato) => {
                console.log("Put receipt:", dato)
                for (let data of dato) {
                  console.log("loop wala7", data);
                  populateModeOfReceipt(data);
                }
                //loop halne
              }, error => {
                console.log("Error on retrieving receipt", error);
              })
          }
        })

        getIdentificationDocument().then(function (data) {
          console.log("Get8: ", data)
          var count = data;
          if (count == 0) {
            http.put("http://doc.cityremit.com/api/master", identificaitonDocumentData)
              .map(res => res.json())
              .subscribe((dato) => {
                console.log("Put document:", dato)
                for (let data of dato) {
                  console.log("loop wala8", data);
                  populateIdentificationDocument(data);
                }
                //loop halne
              }, error => {
                console.log("Error on retrieving document", error);
              })
          }
        })

        getAgentList().then(function (data) {
          console.log("Get9: ", data)
          var count = data;
          if (count == 0) {
            http.put("http://doc.cityremit.com/api/master", agentData)
              .map(res => res.json())
              .subscribe((dato) => {
                console.log("Put agent:", dato)
                for (let data of dato) {
                  console.log("loop wala9", data);
                  populateAgentList(data);
                }
                //loop halne
              }, error => {
                console.log("Error on retrieving agent", error);
              })
          }
        })



      function populateCountries(country){
        console.log("populatedd wala",country)
        //insert
        db.bulkDocs([
          { CountryName: country.CountryName,
            _id: country.CountryID,
            ISO2Code: country.ISO2Code,
            ISO3Code: country.ISO3Code,
            IsActive: country.IsActive,
            Nationality: country.Nationality,
            NumericISOCode: country.NumericISOCode
          }
        ]).then(function (result) {
          // handle result
          console.log("Populated", result)
        }).catch(function (err) {
          console.log("no populated",err);
        });
      }

      function populateDistricts(district){
        console.log("populatedd wala1",district)
        //insert
        db1.bulkDocs([
          { _id: district.CountryID,
            code: district.code,
            countryid: district.countryid,
            id: district.id,
            status: district.status,
            title: district.title
          }
        ]).then(function (result) {
          // handle result
          console.log("Populated1", result)
        }).catch(function (err) {
          console.log("no populated1",err);
        });
      }

      function populatePages(page) {
        console.log("populatedd wala2", page)
        //insert
        db2.bulkDocs([
          {
            content: page.content,
            from_date: page.from_date,
            _id: page.pageid,
            status: page.status,
            title: page.title,
            to_date: page.to_date
          }
        ]).then(function (result) {
          // handle result
          console.log("Populated2", result)
        }).catch(function (err) {
          console.log("no populated2", err);
        });
      }

        function populateNews(news){
          console.log("populatedd wala3",news)
          //insert
          db3.bulkDocs([
            { content: news.content,
              from_date: news.from_date,
              _id: news.newsid,
              status: news.status,
              title: news.title,
              to_date: news.to_date
            }
          ]).then(function (result) {
            // handle result
            console.log("Populated3", result)
          }).catch(function (err) {
            console.log("no populated3",err);
          });
        }

        function populateoccupation(occupation){
          console.log("populatedd wala4",occupation)
          //insert
          db4.bulkDocs([
            { _id: occupation.id,
              status: occupation.status,
              title: occupation.title
            }
          ]).then(function (result) {
            // handle result
            console.log("Populated4", result)
          }).catch(function (err) {
            console.log("no populated4",err);
          });
        }

        function populatePurposeOfRemittance(remitPurpose){
          console.log("populatedd wala5",remitPurpose)
          //insert
          db5.bulkDocs([
            { _id: remitPurpose.id,
              status: remitPurpose.status,
              title: remitPurpose.title
            }
          ]).then(function (result) {
            // handle result
            console.log("Populated5", result)
          }).catch(function (err) {
            console.log("no populated5",err);
          });
        }

        function populateRelationship(relationship){
          console.log("populatedd wala6",relationship)
          //insert
          db6.bulkDocs([
            { _id: relationship.id,
              status: relationship.status,
              title: relationship.title
            }
          ]).then(function (result) {
            // handle result
            console.log("Populated6", result)
          }).catch(function (err) {
            console.log("no populated6",err);
          });
        }

        function populateModeOfReceipt(receiptMode){
          console.log("populatedd wala7",receiptMode)
          //insert
          db7.bulkDocs([
            { _id: receiptMode.id,
              status: receiptMode.status,
              title: receiptMode.title
            }
          ]).then(function (result) {
            // handle result
            console.log("Populated7", result)
          }).catch(function (err) {
            console.log("no populated7",err);
          });
        }

        function populateIdentificationDocument(doc){
          console.log("populatedd wala8",doc)
          //insert
          db8.bulkDocs([
            { _id: doc.id,
              status: doc.status,
              title: doc.title
            }
          ]).then(function (result) {
            // handle result
            console.log("Populated8", result)
          }).catch(function (err) {
            console.log("no populated8",err);
          });
        }

        function populateAgentList(agent){
          console.log("populatedd wala9",agent)
          //insert
          db9.bulkDocs([
            { Address: agent.Address,
              _id: agent.AgentID,
              AgentName: agent.AgentName,
              AgentType: agent.AgentType,
              ContactNumber: agent.ContactNumber,
              CountryID: agent.CountryID,
              CreatedBy: agent.CreatedBy,
              CreatedDate: agent.CreatedDate,
              DistrictID: agent.DistrictID,
              IsActive: agent.IsActive,
              ReferenceCode: agent.ReferenceCode,
              UpdatedBy: agent.UpdatedBy,
              UpdatedDate: agent.UpdatedDate
            }
          ]).then(function (result) {
            // handle result
            console.log("Populated9", result)
          }).catch(function (err) {
            console.log("no populated9",err);
          });
        }


      function populateBeneficiaries(beneficiary){
        console.log("populatedd wala0",beneficiary)
        //insert
        db0.bulkDocs([
          { MemberID : beneficiary.MemberID,
            _id : beneficiary.BenificiaryID,
            FirstName : beneficiary.FirstName,
            MiddleName : beneficiary.MiddleName,
            LastName : beneficiary.LastName,
            ZipCode : beneficiary.ZipCode,
            Address : beneficiary.Address,
            RelationshipTypeID : beneficiary.RelationshipTypeID,
            CountryID : beneficiary.CountryID,
            ContactNumber : beneficiary.ContactNumber,
            ReceiptModeID : beneficiary.ReceiptModeID,
            RemittencePurposeID : beneficiary.RemittencePurposeID
          }
        ]).then(function (result) {
          // handle result
          console.log("Populated0", result)
        }).catch(function (err) {
          console.log("no populated0",err);
        });
      }

      function selectCountries(){
        var countries = [];
        db.allDocs({
          include_docs: true,
          attachments: true
        }).then(function (res) {
          // handle result
          for (var i = 0; i < res.rows.length; i++) {
            countries.push(res.rows[i]);
          }
        }).catch(function (err) {
          console.log(err);
        });
        return countries;
      }


      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

